# soal-shift-sisop-modul-2-ITB06-2022
a. Saat program pertama kali berjalan. Program akan mendownload file characters dan file weapons dari link yang ada dibawah, lalu program akan mengekstrak kedua file tersebut. File tersebut akan digunakan sebagai database untuk melakukan gacha item characters dan weapons. Kemudian akan dibuat sebuah folder dengan nama “gacha_gacha” sebagai working directory. Seluruh hasil gacha akan berada di dalam folder tersebut. Penjelasan sistem gacha ada di poin (d).

b. Mas Refadi ingin agar setiap kali gacha, item characters dan item weapon akan selalu bergantian diambil datanya dari database. Maka untuk setiap kali jumlah-gacha nya bernilai genap akan dilakukan gacha item weapons, jika bernilai ganjil maka item characters. Lalu untuk setiap kali jumlah-gacha nya mod 10, maka akan dibuat sebuah file baru (.txt) dan output hasil gacha selanjutnya akan berada di dalam file baru tersebut. Dan setiap kali jumlah-gacha nya mod 90, maka akan dibuat sebuah folder baru dan file (.txt) selanjutnya akan berada didalam folder baru tersebut.  Sehingga untuk setiap folder, akan terdapat 9 file (.txt) yang didalamnya berisi 10 hasil gacha. Dan karena ini simulasi gacha, maka hasil gacha di dalam file .txt adalah ACAK/RANDOM dan setiap file (.txt) isi nya akan BERBEDA

c. Format penamaan setiap file (.txt) nya adalah {Hh:Mm:Ss}_gacha_{jumlah-gacha}, misal 04:44:12_gacha_120.txt, dan format penamaan untuk setiap folder nya adalah total_gacha_{jumlah-gacha}, misal total_gacha_270. Dan untuk setiap file (.txt) akan memiliki perbedaan penamaan waktu output sebesar 1 second.

d. Pada game tersebut, untuk melakukan gacha item kita harus menggunakan alat tukar yang dinamakan primogems. Satu kali gacha item akan menghabiskan primogems sebanyak 160 primogems. Karena mas Refadi ingin agar hasil simulasi gacha nya terlihat banyak, maka pada program, primogems di awal di-define sebanyak 79000 primogems. Setiap kali gacha, ada 2 properties yang akan diambil dari database, yaitu name dan rarity. Lalu Outpukan hasil gacha nya ke dalam file (.txt) dengan format hasil gacha {jumlah-gacha}_[tipe-item]_{rarity}_{name}_{sisa-primogems}. Program akan selalu melakukan gacha hingga primogems habis.
Contoh : 157_characters_5_Albedo_53880

e. Proses untuk melakukan gacha item akan dimulai bertepatan dengan anniversary pertama kali mas Refadi bermain bengshin impek, yaitu pada 30 Maret jam 04:44.  Kemudian agar hasil gacha nya tidak dilihat oleh teman kos nya, maka 3 jam setelah anniversary tersebut semua isi di folder gacha_gacha akan di zip dengan nama not_safe_for_wibu dengan dipassword "satuduatiga", lalu semua folder akan di delete sehingga hanya menyisakan file (.zip)

### Jawaban

a. Program ini sudah mendeclare beberapa global variable, berikut merupakan global variablenya:

```c++
char type[1000];
int jumlah_gacha = 0;
char json_file[1001];
```

Variable type digunakan untuk mengetahui jenis gacha yang sedang dijalankan (weapon or characters), jumlah gacha digunakan untuk menghitung giliran gacha yang sekarang dan variable json_file digunakan untuk menyimpan penamaan file json pada setiap gacha

Hal pertama yang harus dilakukan oleh program adalah mengdownload file zip dari gdrive yang sudah disediakan. Lalu setelah itu, lakukan unzip file pada setiap file yang didownload.

```c++
    char link[2][1000] = {
            "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download",
            "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download"
        };

    char name_file[2][1000] = {
            "weapons.zip",
            "characters.zip"
    };
    
    if(child_id == 0) {
        for (int i = 0; i < 2; ++i) {
            downloads(link[i], name_file[i]);
            unzip_file(name_file[i]);
        }

```
Dengan function downloads adalah seperti berikut

```c++
void downloads(char link[1000], char name_file[1000]) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_1[] = {"wget", "-q","--no-check-certificate" , link , "-O", name_file, NULL};
        execv("/bin/wget", argv_1);

    } else {
        wait(NULL);
    }
}
```
fungsi donwloads akan menerima 2 argumen, yaitu link gdrive yang akan didownload dan nama file yang akan digunakan pada file yang didownload karena apabila didownload tanpa pemberian nama file yang diinginkan, maka file yang didownload tersebut akan memiliki penamaan file yang random. Untuk menjalankan command seperti pada terminal, maka digunakan `execv()`

Lalu functions unzip_file adalah seperti ini

```c++
void unzip_file(char name_file[1000]) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv_2[] = {"unzip","-q", name_file, NULL};
        execv("/bin/unzip", argv_2);
        exit(EXIT_SUCCESS);

    } else {
        wait(NULL);
    }

}
```

Sama seperti coding pada function downloads, untuk melakukan unzip, dapat dijalankan command `unzip` menggunakan function `execv`.

Kemudian, program akan membuat directory bernama "gacha_gacha" untuk menyimpan hasil dari gacha yang akan dijalankan. Berikut merupakan codingan dari program untuk membuat directory tersebut

```c++
   DIR *gacha_gacha_dir = opendir("gacha_gacha");

    if (gacha_gacha_dir) {
        closedir(gacha_gacha_dir);
    } else  {
        if (child_id == 0) {
            char *argv_gacha_gacha_dir[] = {"mkdir", "gacha_gacha", NULL};
            execv("/bin/mkdir", argv_gacha_gacha_dir);
            exit(EXIT_SUCCESS);
        } else {
            wait(NULL);
        }
    }
```

Program yang dijalankan akan mengecek terlebih dahulu, apakah directory gacha tersebut sudah tersedia ataukah belum. Jika sudah, maka program tersebut tidak akan membuat directory yang sama karena dikhawatirkan akan ada overwrite.

b. Untuk program selanjutnya, akan dibuat program gacha. Gacha akan selalu dihasilkan selama promogems masih mencukupi untuk melakakukan gacha dan hasil gacha akan mengikuti jumlah gacha yang sedang dijalankan. Berikut merupakan algoritma dan coding dari function gacha

```c++
void gacha() {
    ++jumlah_gacha;
    pid_t child_id;
    child_id = fork();
    struct dirent *dp;
    int gacha;
    char path_dir[1000];
    FILE *fp;



    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        if(jumlah_gacha % 2 == 0) {
            strcpy(path_dir, "weapons/");
            strcpy(type, "weapons");
            gacha = rand() % 130;
        } else {
            strcpy(path_dir, "characters/");
            strcpy(type, "characters");
            gacha = rand() % 48;
        }

        DIR *direct = opendir(path_dir);

        if (direct != NULL) {
            for(int i = 0; i <= gacha; ++i) {
                dp = readdir(direct);
            }

        strcat(path_dir, dp->d_name);
        strcpy(json_file, path_dir);

        closedir(direct);

        } else {
            perror("SALAH");
        
        }
            
    } else {
        wait(NULL);
    }
}

```
Urutan file yang diambil dari setiap gacha weapon dan character akan mengikuti hasil modulus dengan jumlah file yang tersedia, seperti 130 untuk weapons karena di file weapons terdiri dari 130 file, begitu pula angka 48 yang didapatkan dari gacha characters. Setelah didapatkan hasil angka gacha, lalu akan dilakukan looping sampai dengan batas gacha yang didapatkan dari angka hasil gacha. Kemudian, file gacha yang didapatkan tersebut merupakan file json yang akan disimpan dalam variable json_file. 

d. Pada poin ini, gacha akan dilakukan looping dengan kondisi batasannya adalah selama promogems yang dimiliki dapat memenuhi minimal promogems menggunakan gacha. Berikut merupakan coding dari penjelasan tersebut

```c++
    int promogems = 79000;
    while (!(promogems < 160)) {

        if (promogems < 160) {
            exit;
        }
        char fil[1000] = {"gacha"};
        FILE *file_gacha;
        char gacha_text[1000];
        char jumlah_gacha_string[100];
        char primogems_string[100];

        for (int i = 1; i <= 10; ++i) {
            if (promogems >= 160) {
                gacha();
                promogems = promogems - 160;
                fp = fopen(json_file, "r");
                fread(buffer, 4096, 1, fp);
                fclose(fp);

                parsed_json = json_tokener_parse(buffer);

                json_object_object_get_ex(parsed_json, "name", &name);
                json_object_object_get_ex(parsed_json, "rarity", &rarity);

                file_gacha = fopen(fil, "a");

                sprintf(jumlah_gacha_string, "%d", jumlah_gacha);
                sprintf(primogems_string, "%d", promogems);

                strcpy(gacha_text, jumlah_gacha_string);
                strcat(gacha_text, "_");
                strcat(gacha_text, type);
                strcat(gacha_text, "_");
                strcat(gacha_text, json_object_get_string(rarity));
                strcat(gacha_text, "_");
                strcat(gacha_text, json_object_get_string(name));
                strcat(gacha_text, "\n");


                fputs(gacha_text, file_gacha);
                fclose(file_gacha);
            } else {
                exit;
            }

        }
```
Selama melakukan gacha, promogems akan berkurang sebanyak 160 poin yang berdasarkan permintaan soal (1 gacha = 160 promogems). Setelah itu, item gacha tersebut akan disimpan menggunakan format nama yang ditentukan dalam soal dan dalam soal tersebut diminta nama dan rarity. Untuk mengakses kedua hal tersebut, ditambahkanlah library json untuk mengakses file json setiap gacha dan mengambil data nama serta rarity pada setiap file json yang didapatkan. Lalu untuk menggabungkan hasil data yang didapatkan untuk menuliskan format nama gacha yang didapat, digunakanlah `strcat()` untuk meng-concatenate setiap string yang ingin kita masukan.

## Soal 2

a. Hal pertama yang perlu dilakukan oleh program adalah mengextract zip yang diberikan ke dalam folder `“/home/[user]/shift2/drakor”`. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. Karena kamu tidak mungkin memeriksa satu-persatu manual, maka program harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
Contoh: Jenis drama korea romance akan disimpan dalam `“/drakor/romance”`, jenis drama korea action akan disimpan dalam `“/drakor/action”` , dan seterusnya.

c. Setelah folder kategori berhasil dibuat, program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama.c. 
Contoh: `“/drakor/romance/start-up.png”`.

d. Karena dalam satu foto bisa terdapat lebih dari satu poster maka foto harus di pindah ke masing-masing kategori yang sesuai. Contoh: foto dengan nama “start-up;2020;romance_the-k2;2016;action.png” dipindah ke folder “/drakor/romance/start-up.png” dan “/drakor/action/the-k2.png”. (note 19/03: jika dalam satu foto ada lebih dari satu poster maka foto tersebut dicopy jadi akhirnya akan jadi 2 foto)

e. Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending). Format harus sesuai contoh dibawah ini.

### Jawaban

a. Pada persoalan 2a, kita diperintahkan untuk melakukan ekstaksi fil zip ke dala, folder `“/home/[user]/shift2/drakor”`. Untuk itu, pertama kita lakukan create folder terlebih dahulu sebagai destinasi tujuan unzip file yang diberikan command seperti berikut.

```c++
if (child_mkdir_shift2 == 0){
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2", NULL};
		execv("/bin/mkdir", argv);
	}
if (child_mkdir_drakor == 0){
		sleep(1);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor", NULL};
		execv("/bin/mkdir", argv);
	}
```
Kemudian agar command setelahnya dapat berjalan, kita gunakan fork() sebagai berikut

```c++
bool is_parent = true;
	if(is_parent) child_mkdir_shift2 = fork();
	
	is_parent = is_parent && child_mkdir_shift2 != 0;
	if(is_parent) child_mkdir_drakor = fork();
	
	is_parent = is_parent && child_mkdir_drakor != 0;
	if(is_parent) child_mkdir_action = fork();

```
Setelah file terbuat, kita lakukan pengekstrakan zip dengan command berikut
```c++
if(child_unzip == 0) {
		char *argv[] = {"unzip", path_to_zip,"-d" ,"/home/fadly/shift2/drakor" , NULL};
		execv("/usr/bin/unzip", argv);
	}
```
keterangan pada `argv[]` terdapat perintah `"unzip"` yang akan melakukan ekstrak file diikuti dengan file destinasi yang akan menjadi tujuan file yang diekstrak di `"/home/fadly/shift2/drakor"`. Setelah semua argumen terpenuhi, maka command akan dieksekusi pada `execv("/usr/bin/unzip", argv)`.

b. Pada soal 2b kita diperintahkan untuk membuat folder untuk destinasi pengelompokan file-file yang sudah diekstrak. sebelumnya kita inisiasi sistem fork sebagai berikut 
```c++
pid_t 
		child_mkdir_shift2,
		child_mkdir_drakor,
		child_mkdir_action,
		child_mkdir_romance,
        child_mkdir_thriller,
        child_mkdir_school,
        child_mkdir_comedy,
        child_mkdir_horror,
        child_mkdir_fantasy,
		child_unzip,
		child_cp_action,
		child_cp_romance,
        child_cp_thriller,
        child_cp_school,
        child_cp_comedy,
        child_cp_horror,
        child_cp_fantasy,
		child_rm_tmp,
		child_rm_drakor;


	bool is_parent = true;
	if(is_parent) child_mkdir_shift2 = fork();
	
	is_parent = is_parent && child_mkdir_shift2 != 0;
	if(is_parent) child_mkdir_drakor = fork();
	
	is_parent = is_parent && child_mkdir_drakor != 0;
	if(is_parent) child_mkdir_action = fork();

	is_parent = is_parent && child_mkdir_action != 0;
	if(is_parent) child_mkdir_romance = fork();

    is_parent = is_parent && child_mkdir_romance != 0;
	if(is_parent) child_mkdir_thriller = fork();

    is_parent = is_parent && child_mkdir_thriller != 0;
	if(is_parent) child_mkdir_school = fork();

    is_parent = is_parent && child_mkdir_school != 0;
	if(is_parent) child_mkdir_comedy = fork();

    is_parent = is_parent && child_mkdir_comedy != 0;
	if(is_parent) child_mkdir_horror = fork();

    is_parent = is_parent && child_mkdir_horror != 0;
	if(is_parent) child_mkdir_fantasy = fork();
    
	is_parent = is_parent && child_mkdir_fantasy != 0;
	if(is_parent) child_unzip = fork();

```
Kemudian untuk command pembuatan filenya dilakukan sama seperti kita membuat file destinasi unzip dengan menggunakan `execv mkdir` 
```c++
if (child_mkdir_action == 0){
		sleep(1);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/action", NULL};
		execv("/bin/mkdir", argv);
	}
	
	if (child_mkdir_romance == 0) {
		sleep(2);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/romance" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_thriller == 0) {
		sleep(3);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/thriller" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_school == 0) {
		sleep(4);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/school" , NULL};
		execv("/bin/mkdir", argv);
	}
	
	if (child_mkdir_comedy == 0) {
		sleep(5);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/comedy" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_horror == 0) {
		sleep(6);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/horror" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_fantasy == 0) {
		sleep(7);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/fantasy" , NULL};
		execv("/bin/mkdir", argv);
	}

```

Pada command diberikan perintah `mkdir` yang disimpan di `argv[]` disertai juga dengan destinasi pembuatan file di dalam file drakor di `/home/fadly/shift2/drakor/...`.

c. Pada soal 2c kita diperintahkan untuk melakukan pemindahan file yang diekstrak dari zip ke dalam kelompoknya sesuai dengan genre yang ada di namefilenya. Dikarekan ada file yang bisa memiliki dua kelompok yang berbeda, maka kita gunakan command copy terlebih dahulu. Supaya dapat mengcopy semua file, kita menggunakan sistem fork() lagi sebagai berikut
```c++
while((wait(&status) > 0));

	is_parent = is_parent && child_unzip != 0;
	if(is_parent) child_cp_action = fork();
	
	is_parent = is_parent && child_cp_action != 0;
	if(is_parent) child_cp_romance = fork();
	
	is_parent = is_parent && child_cp_romance != 0;
	if(is_parent) child_cp_thriller = fork();
	
	is_parent = is_parent && child_cp_thriller != 0;
	if(is_parent) child_cp_school = fork();
	
	is_parent = is_parent && child_cp_school != 0;
	if(is_parent) child_cp_comedy = fork();
	
	is_parent = is_parent && child_cp_comedy != 0;
	if(is_parent) child_cp_horror = fork();
	
	is_parent = is_parent && child_cp_horror != 0;
	if(is_parent) child_cp_fantasy = fork();
	
	is_parent = is_parent && child_cp_fantasy!= 0;
	if(is_parent) child_rm_drakor = fork();
	
	is_parent = is_parent && child_rm_drakor != 0;
	if(is_parent) child_rm_tmp = fork();
	
```

Setelah melakukan inisiasi fork(), maka dapat dilakukan command copy file ke destinasi folder sesuai dengan kelompoknya.
```c++

	if(child_cp_action == 0) {
		sleep(6);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*action*\" -exec cp {} /home/fadly/shift2/drakor/action \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
	
	if(child_cp_romance == 0) {
		sleep(7);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*romance*\" -exec cp {} /home/fadly/shift2/drakor/romance \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	if(child_cp_thriller == 0) {
		sleep(8);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*thriller*\" -exec cp {} /home/fadly/shift2/drakor/thriller \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	if(child_cp_school == 0) {
		sleep(9);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*school*\" -exec cp {} /home/fadly/shift2/drakor/school \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
	
	if(child_cp_comedy== 0) {
		sleep(10);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*comedy*\" -exec cp {} /home/fadly/shift2/drakor/comedy \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
	
	if(child_cp_horror == 0) {
		sleep(11);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*horror*\" -exec cp {} /home/fadly/shift2/drakor/horror \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
	
	if(child_cp_fantasy == 0) {
		sleep(12);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*fantasy*\" -exec cp {} /home/fadly/shift2/drakor/fantasy \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
```

Di dalam `char *argv[]` terdapat beberapa argumen untuk dapat menjalankan command copy file yang ada. Pertama, kita gunakan shll script dengan pada argumen `"sh", "-c"`. Diikuti dengan dimana folder yang menyimpan file yang akan kita olah, file tersebut berada didalam file `drakor`. Jadi kita arahkan ke alamat folder dengan argumen `find /home/fadly/shift2/drakor`. Dikarenakan file akan dikelompokkan berdasarkan character yang ada pada filenamenya, maka kita sorting dengan cara `-type f -iname` diikuti \"*namakelompok*"\. Lalu, untuk destinasi copyfile akan diarahkan ke folder tujuan dengan cara `-exec cp {} /home/fadly/shift2/drakor/comedy`.

## Soal 3

a. Praktikan diminta untuk membuat program 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”. 

b. Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”

c. Kemudian praktikan diminta untuk memisahkan hasil extract menjadi hewan darat dan hewan air dan dimasukan folder sesuai dengan folder darat atau air. dan untuk hewan yang tidak memiliki keterangan akan dihapus

d. Kemudian pada folder hewan darat terdapat beberapa file yang bergambar hewan burung, gambar hewan burung tersebut diminta untuk dihapus

e. Terakhir, praktikan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut.

### Jawaban

a.  Untuk menyelesaikan nomor 3a, pembuatan kedua direktori menggunakan fork() `pid_t` dengan nama `child_mkdir_darat` dan `child_mkdir_air`, Child process akan melakukan mkdir /home/opay/modul2/darat, dan melakukan mkdir /home/opay/modul2/air dalam selang waktu 3 detik dengan menggunakan function `sleep(3)`. Dan pada script yang praktikan buat, menggunakan function `bool` untuk mengecheck setiap proses menjalankan pada parent process atau child process.

```c++
int main() {
	pid_t 
		child_mkdir_darat,
		child_mkdir_air,
		child_unzip,
		child_mv_darat,
		child_mv_air,
		child_rm_tmp,
		child_rm_bird;

	
	bool is_parent = true;
	if(is_parent) child_mkdir_darat = fork();

	is_parent = is_parent && child_mkdir_darat != 0;
	if(is_parent) child_mkdir_air = fork();

	is_parent = is_parent && child_mkdir_air != 0;
	if(is_parent) child_unzip = fork();


	// nomor 1
	if (child_mkdir_darat == 0) {
		char *argv[] = {"mkdir", "-p", "/home/opay/modul2/darat" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_air == 0) {
		sleep(3);
		char *argv[] = {"mkdir", "-p", "/home/opay/modul2/air" , NULL};
		execv("/bin/mkdir", argv);
```
Kemudian command yang digunakan untuk membuat directorynya adalah `char *argv[] = {"mkdir", "-p", "/home/opay/modul2/darat" , NULL}; execv("/bin/mkdir", argv)" ` dengan menggunakan function execv.

b. Kemudian pada soal b praktikan diminta mengunzip file yang diberikan. Pada script yang telah dibuat, praktikan pertama-pertama mendeclare bahwa file yang akan di zip menggunakan variable `path_to_zip` dengan lokasi file zip yuang berada pada `/home/opay/Downloads/animal.zip`. Dan proses ini menggunakan fork() `pid_t` dengan nama `child_unzip`

```c++
	if(child_unzip == 0) {
		char *argv[] = {"unzip", path_to_zip,"-d" ,"/tmp" , NULL};
		execv("/usr/bin/unzip", argv);
	}
```

kemudian command yang digunakan untuk melakukan unzipping files nya adalah `char *argv[] = {"unzip", path_to_zip,"-d" ,"/tmp" , NULL}; execv("/usr/bin/unzip", argv);` 

c. Kemudian pada soal c praktikan diminta untuk memisahkan dan memasukan file hewan sesuai dengan namanya darat atau air pada folder yang sesuai, proses ini dilakukan dengan fork() dengan `pid_t` dengan nama `child_mv_darat` dan `child_mv_air`

```c++
is_parent = is_parent && child_unzip != 0;
	if(is_parent) child_mv_darat = fork();
	
	is_parent = is_parent && child_mv_darat != 0;
	if(is_parent) child_mv_air = fork();
	
	is_parent = is_parent && child_mv_air != 0;
	if(is_parent) child_rm_bird = fork();
	
	is_parent = is_parent && child_rm_bird != 0;
	if(is_parent) child_rm_tmp = fork();
	

	if(child_mv_darat == 0) {
		sleep(2);
		char *argv[] = { "sh", "-c", "find /tmp/animal -type f -iname \"*darat*\" -exec mv {} /home/opay/modul2/darat \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	if(child_mv_air == 0) {
		sleep(2);
		char *argv[] = { "sh", "-c", "find /tmp/animal -type f -iname \"*air*\" -exec mv {} /home/opay/modul2/air \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
    ```

sama seperti poin-poin sebelumnya akan dilakukan pengecheckan terlebih dahulu apakah process dilakukan pada parent process atau child process, kemudian akan dilakukan command `char*argv[] = { "sh", "-c", "find /tmp/animal -type f -iname \"*darat*\" -exec mv {} /home/opay/modul2/darat \\;", NULL }; execv("/usr/bin/sh", argv); jadi akan dilakukan pencarian pada folder `/tmp` dengan nama file yang bernama darat menggunakan `*darat*` setelah itu akan di masukan ke folder darat menggunakan `/home/opay/modul2/darat`. dan untuk yang air dengan cara yang sama.

Dan untuk menghapus sisa folder yang tidak memiliki keterangan nama darat dan air akan dihapus menggunakan command `char *argv[] = { "rm", "-r", "/tmp/animal", NULL}; execv("/usr/bin/rm", argv);` dan berikut dibawah adalah codenya

```c++
	if(child_rm_tmp == 0) {
		sleep(10);
		char *argv[] = { "rm", "-r", "/tmp/animal", NULL};
		execv("/usr/bin/rm", argv);
	}
```


d. Kemudian praktikan diminta untuk menghapus file yang terdapat pada folder darat dan mengandung nama burung/bird menggunakan fork() dengan `pid_t`  dan nama `child_rm_bird`, dan sama seperti sebelumnya akan di check untuk memastikan berjalan pada child process.

```c++
char *argv[] = { "sh", "-c", "find /home/opay/modul2/darat -type f -iname \"*bird*\" -exec rm {} \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
```
sama seperti pada poin c, pertama akan dilakukan pencarian file dengan nama `*bird*` dan akan di remove dengan command `exec rm`  
