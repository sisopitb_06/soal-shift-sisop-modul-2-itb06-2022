#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdbool.h>

char path_to_zip[] = "/home/fadly/Downloads/drakor.zip";

int main() {
	pid_t 
		child_mkdir_shift2,
		child_mkdir_drakor,
		child_mkdir_action,
		child_mkdir_romance,
        child_mkdir_thriller,
        child_mkdir_school,
        child_mkdir_comedy,
        child_mkdir_horror,
        child_mkdir_fantasy,
		child_unzip,
		child_cp_action,
		child_cp_romance,
        child_cp_thriller,
        child_cp_school,
        child_cp_comedy,
        child_cp_horror,
        child_cp_fantasy,
		child_rm_tmp,
		child_rm_drakor;


	bool is_parent = true;
	if(is_parent) child_mkdir_shift2 = fork();
	
	is_parent = is_parent && child_mkdir_shift2 != 0;
	if(is_parent) child_mkdir_drakor = fork();
	
	is_parent = is_parent && child_mkdir_drakor != 0;
	if(is_parent) child_mkdir_action = fork();

	is_parent = is_parent && child_mkdir_action != 0;
	if(is_parent) child_mkdir_romance = fork();

    is_parent = is_parent && child_mkdir_romance != 0;
	if(is_parent) child_mkdir_thriller = fork();

    is_parent = is_parent && child_mkdir_thriller != 0;
	if(is_parent) child_mkdir_school = fork();

    is_parent = is_parent && child_mkdir_school != 0;
	if(is_parent) child_mkdir_comedy = fork();

    is_parent = is_parent && child_mkdir_comedy != 0;
	if(is_parent) child_mkdir_horror = fork();

    is_parent = is_parent && child_mkdir_horror != 0;
	if(is_parent) child_mkdir_fantasy = fork();
    
	is_parent = is_parent && child_mkdir_fantasy != 0;
	if(is_parent) child_unzip = fork();

	if (child_mkdir_shift2 == 0){
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2", NULL};
		execv("/bin/mkdir", argv);
	}
	if (child_mkdir_drakor == 0){
		sleep(1);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor", NULL};
		execv("/bin/mkdir", argv);
	}
	if (child_mkdir_action == 0){
		sleep(1);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/action", NULL};
		execv("/bin/mkdir", argv);
	}
	
	if (child_mkdir_romance == 0) {
		sleep(2);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/romance" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_thriller == 0) {
		sleep(3);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/thriller" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_school == 0) {
		sleep(4);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/school" , NULL};
		execv("/bin/mkdir", argv);
	}
	
	if (child_mkdir_comedy == 0) {
		sleep(5);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/comedy" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_horror == 0) {
		sleep(6);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/horror" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_fantasy == 0) {
		sleep(7);
		char *argv[] = {"mkdir", "-p", "/home/fadly/shift2/drakor/fantasy" , NULL};
		execv("/bin/mkdir", argv);
	}

    int status;	

	if(child_unzip == 0) {
		char *argv[] = {"unzip", path_to_zip,"-d" ,"/home/fadly/shift2/drakor" , NULL};
		execv("/usr/bin/unzip", argv);
	}

	while((wait(&status) > 0));

	is_parent = is_parent && child_unzip != 0;
	if(is_parent) child_cp_action = fork();
	
	is_parent = is_parent && child_cp_action != 0;
	if(is_parent) child_cp_romance = fork();
	
	is_parent = is_parent && child_cp_romance != 0;
	if(is_parent) child_cp_thriller = fork();
	
	is_parent = is_parent && child_cp_thriller != 0;
	if(is_parent) child_cp_school = fork();
	
	is_parent = is_parent && child_cp_school != 0;
	if(is_parent) child_cp_comedy = fork();
	
	is_parent = is_parent && child_cp_comedy != 0;
	if(is_parent) child_cp_horror = fork();
	
	is_parent = is_parent && child_cp_horror != 0;
	if(is_parent) child_cp_fantasy = fork();
	
	is_parent = is_parent && child_cp_fantasy!= 0;
	if(is_parent) child_rm_drakor = fork();
	
	is_parent = is_parent && child_rm_drakor != 0;
	if(is_parent) child_rm_tmp = fork();
	

	if(child_cp_action == 0) {
		sleep(6);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*action*\" -exec cp {} /home/fadly/shift2/drakor/action \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
	
	if(child_cp_romance == 0) {
		sleep(7);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*romance*\" -exec cp {} /home/fadly/shift2/drakor/romance \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	if(child_cp_thriller == 0) {
		sleep(8);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*thriller*\" -exec cp {} /home/fadly/shift2/drakor/thriller \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	if(child_cp_school == 0) {
		sleep(9);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*school*\" -exec cp {} /home/fadly/shift2/drakor/school \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
	
	if(child_cp_comedy== 0) {
		sleep(10);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*comedy*\" -exec cp {} /home/fadly/shift2/drakor/comedy \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
	
	if(child_cp_horror == 0) {
		sleep(11);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*horror*\" -exec cp {} /home/fadly/shift2/drakor/horror \\;", NULL };
		execv("/usr/bin/sh", argv);
	}
	
	if(child_cp_fantasy == 0) {
		sleep(12);
		char *argv[] = { "sh", "-c", "find /home/fadly/shift2/drakor -type f -iname \"*fantasy*\" -exec cp {} /home/fadly/shift2/drakor/fantasy \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	
	return 0;
}
