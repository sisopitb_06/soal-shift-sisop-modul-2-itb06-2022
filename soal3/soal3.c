#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <wait.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <stdbool.h>

char path_to_zip[] = "/home/opay/Downloads/animal.zip";

int main() {
	pid_t 
		child_mkdir_darat,
		child_mkdir_air,
		child_unzip,
		child_mv_darat,
		child_mv_air,
		child_rm_tmp,
		child_rm_bird;

	
	bool is_parent = true;
	if(is_parent) child_mkdir_darat = fork();

	is_parent = is_parent && child_mkdir_darat != 0;
	if(is_parent) child_mkdir_air = fork();

	is_parent = is_parent && child_mkdir_air != 0;
	if(is_parent) child_unzip = fork();


	// nomor 1
	if (child_mkdir_darat == 0) {
		char *argv[] = {"mkdir", "-p", "/home/opay/modul2/darat" , NULL};
		execv("/bin/mkdir", argv);
	}

	if (child_mkdir_air == 0) {
		sleep(3);
		char *argv[] = {"mkdir", "-p", "/home/opay/modul2/air" , NULL};
		execv("/bin/mkdir", argv);
int status;	

	if(child_unzip == 0) {
		char *argv[] = {"unzip", path_to_zip,"-d" ,"/tmp" , NULL};
		execv("/usr/bin/unzip", argv);
	}

	while((wait(&status) > 0));

	is_parent = is_parent && child_unzip != 0;
	if(is_parent) child_mv_darat = fork();
	
	is_parent = is_parent && child_mv_darat != 0;
	if(is_parent) child_mv_air = fork();
	
	is_parent = is_parent && child_mv_air != 0;
	if(is_parent) child_rm_bird = fork();
	
	is_parent = is_parent && child_rm_bird != 0;
	if(is_parent) child_rm_tmp = fork();
	

	if(child_mv_darat == 0) {
		sleep(2);
		char *argv[] = { "sh", "-c", "find /tmp/animal -type f -iname \"*darat*\" -exec mv {} /home/opay/modul2/darat \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	if(child_mv_air == 0) {
		sleep(2);
		char *argv[] = { "sh", "-c", "find /tmp/animal -type f -iname \"*air*\" -exec mv {} /home/opay/modul2/air \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	if(child_rm_bird == 0) {
		sleep(6);
		char *argv[] = { "sh", "-c", "find /home/opay/modul2/darat -type f -iname \"*bird*\" -exec rm {} \\;", NULL };
		execv("/usr/bin/sh", argv);
	}

	if(child_rm_tmp == 0) {
		sleep(10);
		char *argv[] = { "rm", "-r", "/tmp/animal", NULL};
		execv("/usr/bin/rm", argv);
	}

	return 0;
}
