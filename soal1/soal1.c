#include <stdio.h>
#include <curl/curl.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include <math.h>
#include <time.h>
#include <json-c/json.h>

void downloads(char link[1000], char name_file[1000]);
void unzip_file(char name_file[1000]);
void gacha();
char type[1000];
int jumlah_gacha = 0;


char json_file[1001];

int main() {

    struct json_object *parsed_json;
    struct json_object *name;
    struct json_object *rarity;

    char buffer[4096];

    FILE *fp;
    int result;

    pid_t child_id;
    child_id = fork();

    char link[2][1000] = {
            "https://drive.google.com/u/0/uc?id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT&export=download",
            "https://drive.google.com/u/0/uc?id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp&export=download"
        };

    char name_file[2][1000] = {
            "weapons.zip",
            "characters.zip"
    };
    
    if(child_id == 0) {
        for (int i = 0; i < 2; ++i) {
            downloads(link[i], name_file[i]);
            unzip_file(name_file[i]);
        }

        
    DIR *gacha_gacha_dir = opendir("gacha_gacha");

    if (gacha_gacha_dir) {
        closedir(gacha_gacha_dir);
    } else  {
        if (child_id == 0) {
            char *argv_gacha_gacha_dir[] = {"mkdir", "gacha_gacha", NULL};
            execv("/bin/mkdir", argv_gacha_gacha_dir);
            exit(EXIT_SUCCESS);
        } else {
            wait(NULL);
        }
    }


    int promogems = 79000;
    while (!(promogems < 160)) {

        if (promogems < 160) {
            exit;
        }
        char fil[1000] = {"gacha"};
        FILE *file_gacha;
        char gacha_text[1000];
        char jumlah_gacha_string[100];
        char primogems_string[100];

        for (int i = 1; i <= 10; ++i) {
            if (promogems >= 160) {
                gacha();
                promogems = promogems - 160;
                fp = fopen(json_file, "r");
                fread(buffer, 4096, 1, fp);
                fclose(fp);

                parsed_json = json_tokener_parse(buffer);

                json_object_object_get_ex(parsed_json, "name", &name);
                json_object_object_get_ex(parsed_json, "rarity", &rarity);

                file_gacha = fopen(fil, "a");

                sprintf(jumlah_gacha_string, "%d", jumlah_gacha);
                sprintf(primogems_string, "%d", promogems);

                strcpy(gacha_text, jumlah_gacha_string);
                strcat(gacha_text, "_");
                strcat(gacha_text, type);
                strcat(gacha_text, "_");
                strcat(gacha_text, json_object_get_string(rarity));
                strcat(gacha_text, "_");
                strcat(gacha_text, json_object_get_string(name));
                strcat(gacha_text, "\n");


                fputs(gacha_text, file_gacha);
                fclose(file_gacha);
            } else {
                exit;
            }

        }
    }

    }
        
}

void downloads(char link[1000], char name_file[1000]) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {
        char *argv_1[] = {"wget", "-q","--no-check-certificate" , link , "-O", name_file, NULL};
        execv("/bin/wget", argv_1);

    } else {
        wait(NULL);
    }
}

void unzip_file(char name_file[1000]) {
    pid_t child_id;
    child_id = fork();

    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        char *argv_2[] = {"unzip","-q", name_file, NULL};
        execv("/bin/unzip", argv_2);
        exit(EXIT_SUCCESS);

    } else {
        wait(NULL);
    }

}

void gacha() {
    ++jumlah_gacha;
    pid_t child_id;
    child_id = fork();
    struct dirent *dp;
    int gacha;
    char path_dir[1000];
    FILE *fp;



    if(child_id < 0 ) exit(EXIT_FAILURE);

    if(child_id == 0) {

        if(jumlah_gacha % 2 == 0) {
            strcpy(path_dir, "weapons/");
            strcpy(type, "weapons");
            gacha = rand() % 130;
        } else {
            strcpy(path_dir, "characters/");
            strcpy(type, "characters");
            gacha = rand() % 48;
        }

        DIR *direct = opendir(path_dir);

        if (direct != NULL) {
            for(int i = 0; i <= gacha; ++i) {
                dp = readdir(direct);
            }

        strcat(path_dir, dp->d_name);
        strcpy(json_file, path_dir);

        closedir(direct);

        } else {
            perror("SALAH");
        
        }
            
    } else {
        wait(NULL);
    }
}
